# Web Demo: Bounce

This is a demo web application that runs only in the browser. It uses an
HTML5 canvas to draw objects and animate them so they move and bounce off the
borders. There are interactive controls that affect the ball's behavior.

This web app was written using the [Seed](https://seed-rs.org/) framework
version 0.7.

## Compile and run the demo

- [Install Rust](https://www.rust-lang.org/tools/install)
- Install [cargo-make](https://sagiegurari.github.io/cargo-make/): ``cargo install cargo-make``
- Clone this repository and ``cd`` to the top-level directory
- Run ``cargo make start``
- Open your browser to http://127.0.0.1:8000/

## Info

This demo is based on these Seed examples:

- [animation](https://github.com/seed-rs/seed/tree/master/examples/animation)
- [canvas](https://github.com/seed-rs/seed/tree/master/examples/canvas)

Useful doc links:
- [web_sys::CanvasRenderingContext2d](https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html)
- [Rust standard library](https://doc.rust-lang.org/std/index.html)
- [web-sys requestAnimationFrame example](https://rustwasm.github.io/docs/wasm-bindgen/examples/request-animation-frame.html)

## Physics

The information display next to the moving objects demonstrates two laws of nature:

- Conservation of momentum
- Conservation of mass-energy

Notice that the total momentum and total energy numbers do not change from
frame to frame.
