mod view;

use bounce::simulator::{max_f, Arena, Color, Position, Simulator, Velocity};
use log::info;
use rand::{Rng, SeedableRng};
use rand_xoshiro::SplitMix64;
use seed::browser::util::body;
use seed::prelude::{
    streams, wasm_bindgen, web_sys, App, ElRef, Ev, Orders, RenderInfo, Url,
};
use std::cell::Cell;
use web_sys::HtmlCanvasElement;

const PI: f64 = std::f64::consts::PI;

#[derive(Debug)]
pub struct Model {
    sim: Simulator,

    // This is presentation-level stuff that doesn't really belong in a Model.
    // According to the [version 0.7 view docs](https://seed-rs.org/0.7.0/view)
    // "view and view_* functions will have own local state once Seed Hooks are
    // integrated."
    view_count: Cell<usize>,
    is_running_animation: bool,
    prev_timestamp: Option<f64>,
    viewport_width: f64,
    viewport_height: f64,
    canvas: ElRef<HtmlCanvasElement>,
}

impl Default for Model {
    fn default() -> Self {
        let mut sim = Simulator::new(Arena::new(
            -0.5, // left
            0.7,  // top
            0.5,  // right
            -0.7, // bottom
        ));
        let s = 2.0; // speed scale
        let v2 = 0.3156854249492381 * s;
        let v1 = v2 * 0.25;
        let a = PI / 4.0;
        sim.create_ball(
            Position::new(0.0, 0.0),
            Velocity::new(0.0, 0.0),
            0.2, // radius in meters
            1.0, // mass in kilograms
            Color::Red,
        );
        sim.create_ball(
            Position::new(-0.26793786053180507, -0.26793786053180507),
            Velocity::new(v1 * a.cos(), v1 * a.sin()),
            0.1,  // radius in meters
            0.25, // mass in kilograms
            Color::Red,
        );
        sim.create_ball(
            Position::new(0.4, 0.4),
            Velocity::new(-v2 * a.cos(), -v2 * a.sin()),
            0.05,   // radius in meters
            0.0625, // mass in kilograms
            Color::Red,
        );
        Self {
            sim,
            view_count: Cell::new(0),
            is_running_animation: false,
            prev_timestamp: None,
            viewport_width: -1.0,
            viewport_height: -1.0,
            canvas: ElRef::<HtmlCanvasElement>::default(),
        }
    }
}

fn create_diffusion_model() -> Model {
    let (left, bottom, width, height) = (-1.0, -0.5, 2.0, 1.0);
    let mut sim = Simulator::new(Arena::new(
        left,            // left
        bottom + height, // top
        left + width,    // right
        bottom,          // bottom
    ));
    // Create a rectangular array of evenly spaced objects,
    // all with the same speed but different directions.
    let (x_scale, y_scale) = (0.2, 0.2);
    let (x_offset, y_offset) = (x_scale / 2.0, y_scale / 2.0);
    let r = x_offset / 3.0;
    let base_speed = 0.2;
    let mut rng: SplitMix64 = SeedableRng::seed_from_u64(1776);
    let mut create_balls = |x_orig, y_orig, color| {
        for j in 0..5 {
            for i in 0..5 {
                let x = x_orig + (i as f64 * x_scale) + x_offset;
                let y = y_orig + (j as f64 * y_scale) + y_offset;
                let a: f64 = rng.gen_range(0.0, 2.0 * PI);
                let vx = base_speed * a.cos();
                let vy = base_speed * a.sin();
                sim.create_ball(
                    Position::new(x, y),
                    Velocity::new(vx, vy),
                    r,   // radius in meters
                    0.1, // mass in kilograms
                    color,
                );
            }
        }
    };
    create_balls(left, bottom, Color::Blue);
    create_balls(left + (width / 2.0), bottom, Color::Green);
    Model {
        sim,
        ..Model::default()
    }
}

fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders
        .send_msg(Msg::SetViewportSize)
        .stream(streams::window_event(Ev::Resize, |_| Msg::SetViewportSize))
        .after_next_render(Msg::Rendered);

    // Model::default()
    create_diffusion_model()
}

#[derive(Copy, Clone)]
pub enum Msg {
    Rendered(RenderInfo),
    SetViewportSize,
    Start,
    Stop,
    Step,
}

fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::Rendered(render_info) => {
            let client_width = f64::from(body().client_width());
            let client_height = f64::from(body().client_height());
            model.viewport_width = client_width;
            model.viewport_height = client_height;
            crate::view::draw(&model.canvas, model);
            orders.after_next_render(Msg::Rendered);
            if model.is_running_animation {
                let cur_timestamp = render_info.timestamp;
                let delta_t = match model.prev_timestamp {
                    Some(prev_timestamp) => {
                        // seconds
                        max_f((cur_timestamp - prev_timestamp) / 1000.0, 0.0)
                    }
                    None => {
                        1.0 / 60.0 // seconds
                    }
                };
                model.sim.update(delta_t);
                model.prev_timestamp = Some(cur_timestamp);
            } else {
                // Don't schedule another render, stop animating.
                orders.skip();
                model.prev_timestamp.take();
            }
        }
        Msg::SetViewportSize => {
            let client_width = f64::from(body().client_width());
            let client_height = f64::from(body().client_height());
            info!(
                "SetViewportSize: client width={}, height={}",
                client_width, client_height
            );
            model.viewport_width = client_width;
            model.viewport_height = client_height;
        }
        Msg::Start => {
            model.is_running_animation = true;
        }
        Msg::Stop => {
            model.is_running_animation = false;
            model.prev_timestamp.take();
        }
        Msg::Step => {
            let delta_t = 1.0 / 60.0; // seconds
            model.sim.update(delta_t);
        }
    }
}

#[wasm_bindgen(start)]
pub fn start() {
    wasm_logger::init(wasm_logger::Config::default());
    App::start("app", init, update, crate::view::view);
}
