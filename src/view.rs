use crate::{Model, Msg};
use bounce::simulator::{Arena, Ball};
use log::info;
use seed::{prelude::*, *}; // element macros and basic Seed types
use web_sys::{CanvasRenderingContext2d, HtmlCanvasElement};

const PI: f64 = std::f64::consts::PI;

pub fn view(model: &Model) -> Node<Msg> {
    let view_count = model.view_count.get() + 1;
    model.view_count.set(view_count);
    let (canvas_width, canvas_height) = if model.viewport_height < 1.0 {
        // Sometimes on first view, viewport height is reported as zero.
        // (This seems to depend on CSS settings.)
        (model.viewport_width, 100.0)
    } else {
        calc_canvas_size(
            &model.sim.arena,
            model.viewport_width,
            model.viewport_height,
        )
    };
    if view_count <= 2 {
        info!(
            "view(): view_count: {}, viewport: ({}, {}), canvas: ({}, {})",
            view_count,
            model.viewport_width,
            model.viewport_height,
            canvas_width,
            canvas_height
        );
    }
    let canvas = &model.canvas;

    div![
        style! {St::Display => "flex"},
        canvas![
            el_ref(canvas),
            attrs![
                At::Width => px(canvas_width),
                At::Height => px(canvas_height),
            ],
            style![
                St::Border => "1px solid black",
            ],
        ],
        div![
            pre![style! {St::Width => "22"}, model.sim.report(),],
            IF!(!model.is_running_animation => button![
                style! {St::Display => "block"},
                "▶ Start", ev(Ev::Click, |_| Msg::Start),
            ]),
            IF!(model.is_running_animation => button![
                style! {St::Display => "block"},
                "⏸ Stop", ev(Ev::Click, |_| Msg::Stop),
            ]),
            IF!(!model.is_running_animation => button![
                style! {St::Display => "block"},
                "Step", ev(Ev::Click, |_| Msg::Step),
            ]),
        ],
    ]
}

fn calc_canvas_size(
    arena: &Arena,
    viewport_width: f64,
    viewport_height: f64,
) -> (f64, f64) {
    if !arena.is_valid() {
        // If arena doesn't have valid dimensions, can't compute canvas size.
        return (1.0, 1.0);
    }
    let avail_width = if viewport_width < 1.0 {
        1.0
    } else {
        // Make 80% of total width available for canvas
        (viewport_width * 0.8).ceil()
    };
    let avail_height = if viewport_height < 1.0 {
        1.0
    } else {
        // Make all of viewport height avaialable except for a few pixels.
        // If I don't make that adjustment then I get a vertical scrollbar.
        // This is probably due to CSS padding or something like that.
        let padding = 15.0;
        if viewport_height > padding {
            (viewport_height - padding).ceil()
        } else {
            viewport_height.ceil()
        }
    };

    // Fit entire canvas in available size without scrollbars
    // while maintaining the arena aspect ratio.
    let q = avail_width / avail_height;
    let r = arena.width() / arena.height();
    let (canvas_width, canvas_height);
    if r < q {
        // Fit canvas to available height
        canvas_height = avail_height;
        canvas_width = canvas_height * r;
    } else {
        // Fit canvas to available width
        canvas_width = avail_width;
        canvas_height = canvas_width / r;
    }
    (canvas_width, canvas_height)
}

pub fn draw(canvas: &ElRef<HtmlCanvasElement>, model: &Model) {
    let canvas = match canvas.get() {
        Some(canvas_elem) => canvas_elem,
        None => {
            info!("draw(): no canvas element yet");
            return;
        }
    };
    let canvas_width = canvas.width() as f64;
    let canvas_height = canvas.height() as f64;
    if canvas_width <= 1.0 || canvas_height <= 1.0 {
        info!("draw(): canvas not sized yet");
        return;
    }

    let ctx = seed::canvas_context_2d(&canvas);
    ctx.clear_rect(0.0, 0.0, canvas_width, canvas_height);

    let pixels_per_meter = canvas_width / model.sim.arena.width();
    for ball in model.sim.balls.iter() {
        draw_ball(&ctx, ball, canvas_width, canvas_height, pixels_per_meter);
    }
}

fn draw_ball(
    ctx: &CanvasRenderingContext2d,
    ball: &Ball,
    canvas_width: f64,
    canvas_height: f64,
    pixels_per_meter: f64,
) {
    let cx = canvas_width / 2.0;
    let cy = canvas_height / 2.0;
    let x = cx + (ball.x() * pixels_per_meter);
    let y = cy - (ball.y() * pixels_per_meter);
    let r = ball.r() * pixels_per_meter;

    ctx.begin_path();
    ctx.set_stroke_style(&JsValue::from_str("black"));
    ctx.set_fill_style(&JsValue::from_str(ball.color().as_str()));
    ctx.arc(x, y, r, 0.0, 2.0 * PI).expect("draw circle");
    ctx.fill();
    ctx.stroke();
}
